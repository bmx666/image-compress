#!/bin/bash

declare TMPDIR

function clean_tmp() {
	rm -fr "${TMPDIR}"
}

if [ ! -f "${1}" ]
then
	echo "usage: ./image-compress.sh input_image [output_image]"
	exit 1
fi

declare compress_filename

if [ -n "${2}" ]
then
	compress_filename="${2}"
fi

TMPDIR=$(mktemp -d)

mimetype=$(file -b --mime-type "${1}")

if [ -z "${mimetype}" ]
then
	echo "invalid mime-type of file"
	clean_tmp
	exit 1
else
	echo "MIME-TYPE: ${mimetype}"
fi

filedir=$(dirname "${1}")
filename=$(basename "${1}")
fake_filename="${filename}"
extension="${filename##*.}"
if [ "${extension}" == "${filename}" ]
then
	extension=''
	fake_filename="${fake_filename}.${mimetype##*/}"
fi

echo "Generate POST data..."
echo -ne "-----------------------------13327280648030\r\n" > "${TMPDIR}/post.data"
echo -ne "Content-Disposition: form-data; name=\"files[]\"; filename=\"${fake_filename}\"\r\n" >> "${TMPDIR}/post.data"
echo -ne "Content-Type: ${mimetype}\r\n" >> "${TMPDIR}/post.data"
echo -ne "\r\n" >> "${TMPDIR}/post.data"

cat "${1}" >> "${TMPDIR}/post.data"

echo -ne "\r\n" >> "${TMPDIR}/post.data"
echo -ne "-----------------------------13327280648030--\r\n" >> "${TMPDIR}/post.data"

curl -s \
	--referer "https://compressor.io/compress" \
	-c "${TMPDIR}/compressor.cookie" \
	-X POST \
	-H 'Pragma: no-cache' \
	-H 'Cache-Control: no-cache' \
	-H 'Accept: application/json, text/javascript, */*; q=0.01' \
	-H 'X-Requested-With: XMLHttpRequest' \
	-H "Content-Type: multipart/form-data; boundary=---------------------------13327280648030" \
	--data-binary @"${TMPDIR}/post.data" \
	"https://compressor.io/server/Lossy.php" \
	> "${TMPDIR}/responce.json"

if [[ $? -ne 0 ]]
then
	clean_tmp
	echo "Upload image failed"
	exit 1
fi

URL=$(cat "${TMPDIR}/responce.json" | sed 's|.*"url":"||g;s|".*||g;s|\\||g')

if [ -z "${compress_filename}" ]
then
	if [ ! -z "${extension}" ]
	then
		filename_pure=$(basename "${filename}" ".${extension}")
		compress_filename="${filename_pure}-compress.${extension}"
	else
		compress_filename="${filename}-compress"
	fi
	compress_filename="${filedir}/${compress_filename}"
fi

curl -s \
	--referer "https://compressor.io/compress" \
	-b "${TMPDIR}/compressor.cookie" \
	"${URL}" \
	> "${compress_filename}"

if [[ $? -ne 0 ]]
then
	rm -f "${compress_filename}"
	clean_tmp
	echo "Fetch compressed image failed"
	exit 1
fi

clean_tmp
echo "Compressed image location: \"${compress_filename}\""
